package bal.client;
import entites.Client;
import java.io.Serializable;
import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;
import static utilitaires.UtilDate.*;

@Singleton 
@Alternative

public class BalClientMock  implements BalClient, Serializable{
    
    
    @Override
    public Float                 caAnnuel(Client pClient, int pAnnee) {
        Float ca=0f;
        for(int m= 1; m<=12;m++) { ca+=caMensuel(null,2014,m);}
        return ca;
    }

    @Override
    public Float                 caMensuel(Client pClient, int pAnnee, int pMois) {
        
        
        Float ca=0f;
        switch(pMois){
            case  1: ca=200f;break; 
            case  2: ca=150f;break; 
            case  3: ca=250f;break; 
            case  4: ca=300f;break;
            case  5: ca=220f;break; 
            case  6: ca=380f;break;
            case  7: ca=200f;break; 
            case  8: ca=100f;break; 
            case  9: ca=350f;break; 
            case 10: ca=250f;break;
            case 11: ca=400f;break; 
            case 12: ca=300f;break;         
        }
        return ca;
    }                

    
    @Override
    public Float caAnneeEnCours(Client pClient) {
       return caAnnuel(pClient,anneeCourante());
    }

    @Override
    public Float caMoisEnCours(Client pClient) {
        return caMensuel(pClient,anneeCourante(), moisCourant());
    }

    @Override
    public Float resteARegler(Client pClient) {
        return 0F;
    }

    

    @Override
    public Float soldeClient(Client pClient) {
       return -resteARegler(pClient);
    }
}
