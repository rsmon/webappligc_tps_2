
package bal.lignedecommande;

import dao.tva.DaoTva;
import entites.LigneDeCommande;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class BalLigneDeCommandeImpl implements BalLigneDeCommande, Serializable {
  
    @Inject DaoTva daoTva;
    
    @Override
    public Float montantHtLigne(LigneDeCommande ldc){
    
     return ldc.getLeProduit().getPrixProd()*ldc.getQteCom();
    }
    
    @Override
    public Float montantTTCLigne(LigneDeCommande ldc){
 
     return montantHtLigne(ldc)*(1+daoTva.getTauxTVA());
    }
}
