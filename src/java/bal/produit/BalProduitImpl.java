
package bal.produit;
import bal.commande.BalCommande;
import dao.produit.DaoProduit;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;
import java.io.Serializable;
import java.util.Date;
import javax.inject.Inject;
import javax.inject.Singleton;
import static utilitaires.UtilDate.*;

@Singleton
public class BalProduitImpl  implements BalProduit, Serializable{
    
    @Inject DaoProduit  daoProduit;
    @Inject BalCommande balCommande;
    
    @Override
    public Float caAnnuelProduit(Produit pProduit, int pAnnee) {
        
        Float ca=0F;
        
        for (LigneDeCommande lgdc: pProduit.getLesLignesDeCommande()){
        
          Commande cmd     = lgdc.getLaCommande();
          Date     dateCmd = cmd.getDateCom();
          String   etatCmd = cmd.getEtatCom();
          
          if( annee(dateCmd)==pAnnee && etatCmd.equalsIgnoreCase("R") ){
          
            ca+=lgdc.getQteCom()*lgdc.getLeProduit().getPrixProd();
          }
          
            
        }
        return ca;
    }
    
    @Override
    public Float caMensuelProduit(Produit pProduit, int pAnnee, int pMois) {
         
        Float ca=0F;
        
        for (LigneDeCommande lgdc: pProduit.getLesLignesDeCommande()){
        
          Commande cmd     = lgdc.getLaCommande();
          Date     dateCmd = cmd.getDateCom();
          String   etatCmd = cmd.getEtatCom();
          
          if( dansAnneeEtMois(dateCmd, pAnnee, pMois) 
              &&  
              etatCmd.equalsIgnoreCase("R")
            )
          {
          
             ca+=lgdc.getQteCom()*lgdc.getLeProduit().getPrixProd();
          }
               
        }
        return ca;
    }
    
    @Override
    public Float caAnneeEnCoursProduit(Produit pProduit){
    
      return caAnnuelProduit( pProduit, anneeCourante());
    }
     
    @Override
    public Float caMoisEnCoursProduit(Produit pProduit){
      
      return caMensuelProduit(pProduit, anneeCourante(), moisCourant());
    }
    
}
