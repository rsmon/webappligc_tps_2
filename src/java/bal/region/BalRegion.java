package bal.region;

import entites.Region;

/**
 *
 * @author rsmon
 */
public interface BalRegion {
    
    Float caAnnuel(Region region, int pAnnee );
    Float caAnneeEnCours(Region region);
}
