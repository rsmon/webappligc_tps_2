package fabrique.dto.client;

import dto.client.ResumeClient;
import entites.Client;
import java.util.List;

public interface FabResumeClient {

    ResumeClient       getResumeClient(Client pClient);

    ResumeClient       getResumeClient(Long pNumcli);   

    List<ResumeClient> getTousLesResumeClients();   
}
