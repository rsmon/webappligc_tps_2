
package fabrique.entite.client;

import entites.Client;
import entites.Region;
import javax.inject.Singleton;

/**
 *
 * @author rsmon
 */

@Singleton
public class FabEntiteClient {
     
    public Client creerEntiteClient(Long numCli,String nomCli, String adrCli, Region laRegion) {
        
        Client client= new Client();  
         
        client.setNumCli(numCli);
        client.setNomCli(nomCli);
        client.setAdrCli(adrCli);
      
        client.setLaRegion(laRegion);
        
        return client;
    } 
}
