
package fabrique.entite.produit;

import entites.CategorieProduit;
import entites.Produit;
import javax.inject.Singleton;

/**
 *
 * @author rsmon
 */
@Singleton
public class FabEntiteProduit {
    
    public Produit creerProduit(
             String refProd,String desigProd, Float prixProd, CategorieProduit categProd
    )
    {
       
        Produit produit= new Produit();
        
        produit.setRefProd(refProd);
        produit.setDesigProd(desigProd);
        produit.setPrixProd(prixProd);
        produit.setLaCategorie(categProd);
       
                
        return produit;
    }
    
}
