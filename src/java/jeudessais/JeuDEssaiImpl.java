package jeudessais;

import dao.Dao;
import entites.CategorieProduit;
import entites.Client;
import entites.Commande;
import entites.Produit;
import entites.Region;
import fabrique.entites.FabEntite;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JeuDEssaiImpl implements JeuDEssai {

    @Inject Dao dao;
    
    @Inject FabEntite fabEntites;
    
    @Override
    public void   remplirBDD() {
        
     Region rPdc,rCa,rPic;  
     
     rPdc = fabEntites.creerEntiteRegion("NPDC","Nord Pas De Calais");
     rCa  = fabEntites.creerEntiteRegion("CA","Champagne Ardennes");
     rPic = fabEntites.creerEntiteRegion("PIC","Picardie");
     
     Client cl101, cl102, cl103, cl104, cl105;
   
     cl101 = fabEntites.creerEntiteClient(101L,"Dupont Jean","Arras",rPdc);
     cl102 = fabEntites.creerEntiteClient(102L,"Legrand Martin","Hirson",rPic);
     cl103 = fabEntites.creerEntiteClient(103L,"Martin Sophie","Calais",rPdc);
     cl104 = fabEntites.creerEntiteClient(104L,"Lemortier Jean-Pierre","Lens",rPdc);
     cl105 = fabEntites.creerEntiteClient(105L,"Laurent Fabrice","Rouvroi",rCa);
          
     Commande cmd34878,cmd34526,cmd34600,cmd34650,cmd35000,cmd35010,cmd35020,cmd35030,cmd35040;
     
     cmd34878= fabEntites.creerCommande(34878L,"07/05/2014","R",cl101);
     cmd34526= fabEntites.creerCommande(34526L,"12/01/2014","R",cl102);
     cmd34600= fabEntites.creerCommande(34600L,"19/05/2014","R",cl101);
     cmd34650= fabEntites.creerCommande(34650L,"09/06/2014","R",cl101);
     cmd35000= fabEntites.creerCommande(35000L,"10/06/2014","R",cl101);
     cmd35010= fabEntites.creerCommande(35010L,"10/06/2014","L",cl101);
     cmd35020= fabEntites.creerCommande(35020L,"12/06/2014","L",cl101);
     cmd35030= fabEntites.creerCommande(35030L,"14/06/2014","R",cl105);
     cmd35040= fabEntites.creerCommande(35040L,"24/10/2014","R",cl104);

     CategorieProduit cpGem,cpPem,cpInf;
      
     cpGem = fabEntites.creerCategorieProduit("GEM","Gros Electro-Ménager");
     cpPem = fabEntites.creerCategorieProduit("PEM","Petit Electro-Ménager");
     cpInf = fabEntites.creerCategorieProduit("INF","Informatique");
     
     Produit pRb1,pLv1,pLv2,pTbl1,pLvv1,pEp1,pImpl1;

     pRb1  = fabEntites.creerProduit("RBM1","Robot Mixer",167f,cpPem);
     pLv1  = fabEntites.creerProduit("LL1","Lave Linge AAA",455f,cpGem);
     pLv2  = fabEntites.creerProduit("LL2","Lave Linge BBB",564f,cpGem);
     pTbl1 = fabEntites.creerProduit("TBL1","Tablette FG",167f,cpInf);
     pLvv1 = fabEntites.creerProduit("LV1","Lave Vaiselle",765f,cpGem);
     pEp1  = fabEntites.creerProduit("EP1","Ecran plat LCD HHHH",145.5f,cpInf);
     pImpl1= fabEntites.creerProduit("IMPL1","Imprimante Laser",345.8f,cpInf);
       
     fabEntites.creerLigneDeCommande(cmd34526,pLv2,1f);
     fabEntites.creerLigneDeCommande(cmd34878,pTbl1,2f);
     fabEntites.creerLigneDeCommande(cmd34526,pRb1,1f);
     fabEntites.creerLigneDeCommande(cmd34600,pTbl1,1f);
     
     fabEntites.creerLigneDeCommande(cmd34650,pLv1,1f);
     fabEntites.creerLigneDeCommande(cmd35000,pLvv1,1f);
     fabEntites.creerLigneDeCommande(cmd35010,pImpl1,2f);
     fabEntites.creerLigneDeCommande(cmd35020,pEp1,1f);
     
     fabEntites.creerLigneDeCommande(cmd35030,pTbl1,1f);
     fabEntites.creerLigneDeCommande(cmd35030,pEp1,1f);
     fabEntites.creerLigneDeCommande(cmd35040,pLv1,1f);
     fabEntites.creerLigneDeCommande(cmd35040,pEp1,1f);
 
     dao.enregistrerEntite(rPdc);
     dao.enregistrerEntite(rCa);
     dao.enregistrerEntite(rPic);
     
     dao.enregistrerEntite(cpGem);
     dao.enregistrerEntite(cpPem);
     dao.enregistrerEntite(cpInf);
 
    }

    @Override
    public void viderBDD() {
        
      dao.debuterTransaction();
        
      dao.getEm().createNativeQuery("delete from LigneDeCommande").executeUpdate();
      dao.getEm().createNativeQuery("delete from Commande").executeUpdate();
      dao.getEm().createNativeQuery("delete from Produit").executeUpdate();
      dao.getEm().createNativeQuery("delete from Client").executeUpdate();
      dao.getEm().createNativeQuery("delete from Region").executeUpdate();
      dao.getEm().createNativeQuery("delete from CategorieProduit").executeUpdate();
       
      dao.validerTransaction();
    }

    @Override
    public String reinitialiserBDD() {
        
      viderBDD();
      remplirBDD();
      
      return "Base de donnees reinitialisee( via EclipseLink vidage en natif , remplissage avec persist )";
        
    }
    
}
