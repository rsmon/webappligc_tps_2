/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice.soap.pourtests.commande;

import bal.commande.BalCommande;
import dao.commande.DaoCommande;
import entites.Commande;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author rsmon
 */
@WebService(serviceName = "WebServiceTesteurCommande")
public class WebServiceTesteurCommande {

    @Inject BalCommande  balcom;
    @Inject DaoCommande  daocom;  

    /**
     * Web service operation
     */
    @WebMethod(operationName = "montantCommandeHT")
    public Float montantCommandeHT(@WebParam(name = "pNumCom") Long pNumCom) {
        
        Commande cmd=daocom.getCommande(pNumCom); 
        return balcom.montantCommandeHT(cmd);
    }
    
    
}
